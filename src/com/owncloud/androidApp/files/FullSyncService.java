/* ownCloud Android client application
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2,
 *   as published by the Free Software Foundation.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.owncloud.androidApp.files;

import android.accounts.Account;
import android.annotation.TargetApi;
import android.app.Service;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SyncRequest;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;

import com.owncloud.androidApp.MainApp;
import com.owncloud.androidApp.authentication.AccountUtils;
import com.owncloud.androidApp.utils.Log_OC;

public class FullSyncService extends Service {
    
    private static final String TAG = FullSyncService.class.getSimpleName();

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
	@Override
    public void onStart(Intent intent, int startId) {
        Log_OC.e(TAG, "Got to start sync");
        Account account = AccountUtils.getCurrentOwnCloudAccount(MainApp.getAppContext());
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.KITKAT) {
            Log_OC.e(TAG, "Canceling all syncs for " + MainApp.getAuthority());
            ContentResolver.cancelSync(null, MainApp.getAuthority());
            Bundle bundle = new Bundle();
            bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
            bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
            Log_OC.e(TAG, "Requesting sync for " + account.name + " at " + MainApp.getAuthority());
            ContentResolver.requestSync(account, MainApp.getAuthority(), bundle);
        } else {
            Log_OC.e(TAG, "Requesting sync for " + account.name + " at " + MainApp.getAuthority()
                    + " with new API");
            SyncRequest.Builder builder = new SyncRequest.Builder();
            builder.setSyncAdapter(account, MainApp.getAuthority());
            builder.setExpedited(true);
            builder.setManual(true);
            builder.syncOnce();
            SyncRequest request = builder.build();
            ContentResolver.requestSync(request);
        }
    }
}
