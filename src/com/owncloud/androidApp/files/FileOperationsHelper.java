/* ownCloud Android client application
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2,
 *   as published by the Free Software Foundation.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.owncloud.androidApp.files;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.accounts.AccountManager;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.DialogFragment;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import com.owncloud.android.lib.common.accounts.AccountUtils.Constants;
import com.owncloud.android.lib.common.network.WebdavUtils;
import com.owncloud.android.lib.resources.status.OwnCloudVersion;
import com.owncloud.androidApp.AnalyticsTracker;
import com.owncloud.androidApp.R;
import com.owncloud.androidApp.datamodel.OCFile;
import com.owncloud.androidApp.services.OperationsService;
import com.owncloud.androidApp.ui.activity.FileActivity;
import com.owncloud.androidApp.ui.dialog.ShareLinkToDialog;
import com.owncloud.androidApp.utils.Log_OC;

/**
 * 
 * @author masensio
 * @author David A. Velasco
 */

public class FileOperationsHelper {
    private static final String TAG = FileOperationsHelper.class.getName();
    
    private static final String FTAG_CHOOSER_DIALOG = "CHOOSER_DIALOG"; 
    

    // Identifier of operation in progress which result shouldn't be lost 
    private long mWaitingForOpId = Long.MAX_VALUE;


    
    public void openFile(OCFile file, FileActivity callerActivity) {
        if (file != null) {
            String storagePath = file.getStoragePath();
            String encodedStoragePath = WebdavUtils.encodePath(storagePath);
            
            Intent intentForSavedMimeType = new Intent(Intent.ACTION_VIEW);
            intentForSavedMimeType.setDataAndType(Uri.parse("file://"+ encodedStoragePath), file.getMimetype());
            intentForSavedMimeType.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            
            Intent intentForGuessedMimeType = null;
            if (storagePath.lastIndexOf('.') >= 0) {
                String guessedMimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(storagePath.substring(storagePath.lastIndexOf('.') + 1));
                if (guessedMimeType != null && !guessedMimeType.equals(file.getMimetype())) {
                    intentForGuessedMimeType = new Intent(Intent.ACTION_VIEW);
                    intentForGuessedMimeType.setDataAndType(Uri.parse("file://"+ encodedStoragePath), guessedMimeType);
                    intentForGuessedMimeType.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                }
            }
            
            Intent chooserIntent = null;
            if (intentForGuessedMimeType != null) {
                chooserIntent = Intent.createChooser(intentForGuessedMimeType, callerActivity.getString(R.string.actionbar_open_with));
            } else {
                chooserIntent = Intent.createChooser(intentForSavedMimeType, callerActivity.getString(R.string.actionbar_open_with));
            }
            
            callerActivity.startActivity(chooserIntent);
            
        } else {
            Log_OC.wtf(TAG, "Trying to open a NULL OCFile");
        }
    }
    

    public void shareFileWithLink(OCFile file, FileActivity callerActivity) {
        if (isSharedSupported(callerActivity)) {
            if (file != null) {
                String link = "https://fake.url";
                
                
                Intent intent = createShareWithLinkIntent(link);                
                String[] packagesToExclude = new String[] { callerActivity.getPackageName() };
                DialogFragment chooserDialog = ShareLinkToDialog.newInstance(intent, packagesToExclude, file);
                chooserDialog.show(callerActivity.getSupportFragmentManager(), FTAG_CHOOSER_DIALOG);
                
            } else {
                Log_OC.wtf(TAG, "Trying to share a NULL OCFile");
            }
            
        } else {
            // Show a Message
            Toast t = Toast.makeText(callerActivity, callerActivity.getString(R.string.share_link_no_support_share_api), Toast.LENGTH_LONG);
            t.show();
        }
    }
    
    public void shareFileWithLinkAsQR(OCFile file, FileActivity callerActivity) {
        if (isSharedSupported(callerActivity)) {
            if (file != null) {
                String link = "https://fake.url";
                
                Intent intent = createShareWithLinkAsQRIntent(link);                
                String[] packagesToExclude = new String[] { callerActivity.getPackageName() };
                DialogFragment chooserDialog = ShareLinkToDialog.newInstance(intent, packagesToExclude, file);
                chooserDialog.show(callerActivity.getSupportFragmentManager(), FTAG_CHOOSER_DIALOG);
            } else {
                Log_OC.wtf(TAG, "Trying to share a NULL OCFile");
            }
        } else {
            // Show a Message
            Toast t = Toast.makeText(callerActivity, callerActivity.getString(R.string.share_link_no_support_share_api), Toast.LENGTH_LONG);
            t.show();
        }
    }
    
    
    public void shareFileWithLinkToApp(OCFile file, Intent sendIntent, FileActivity callerActivity) {
        
        if (file != null) {
            callerActivity.showLoadingDialog();
            
            Intent service = new Intent(callerActivity, OperationsService.class);
            service.setAction(OperationsService.ACTION_CREATE_SHARE);
            service.putExtra(OperationsService.EXTRA_ACCOUNT, callerActivity.getAccount());
            service.putExtra(OperationsService.EXTRA_REMOTE_PATH, file.getRemotePath());
            service.putExtra(OperationsService.EXTRA_SEND_INTENT, sendIntent);
            mWaitingForOpId = callerActivity.getOperationsServiceBinder().newOperation(service);
            
        } else {
            Log_OC.wtf(TAG, "Trying to open a NULL OCFile");
        }
    }
    
    
    private Intent createShareWithLinkIntent(String link) {
        Intent intentToShareLink = new Intent(Intent.ACTION_SEND);
        intentToShareLink.putExtra(Intent.EXTRA_TEXT, link);
        intentToShareLink.setType(HTTP.PLAIN_TEXT_TYPE);
        return intentToShareLink; 
    }
    
    private Intent createShareWithLinkAsQRIntent(String link) {
        Intent intentToShareLink = new Intent(Intent.ACTION_SEND);
        intentToShareLink.putExtra(Intent.EXTRA_TEXT, link);
        intentToShareLink.setType("image/png");
        return intentToShareLink; 
    }
    
    
    /**
     *  @return 'True' if the server supports the Share API
     */
    public boolean isSharedSupported(FileActivity callerActivity) {
        if (callerActivity.getAccount() != null) {
            AccountManager accountManager = AccountManager.get(callerActivity);

            String version = accountManager.getUserData(callerActivity.getAccount(), Constants.KEY_OC_VERSION);
            return (new OwnCloudVersion(version)).isSharedSupported();
            //return Boolean.parseBoolean(accountManager.getUserData(callerActivity.getAccount(), OwnCloudAccount.Constants.KEY_SUPPORTS_SHARE_API));
        }
        return false;
    }
    
    
    public void unshareFileWithLink(OCFile file, FileActivity callerActivity) {
        
        if (isSharedSupported(callerActivity)) {
            // Unshare the file
            Intent service = new Intent(callerActivity, OperationsService.class);
            service.setAction(OperationsService.ACTION_UNSHARE);
            service.putExtra(OperationsService.EXTRA_ACCOUNT, callerActivity.getAccount());
            service.putExtra(OperationsService.EXTRA_REMOTE_PATH, file.getRemotePath());
            mWaitingForOpId = callerActivity.getOperationsServiceBinder().newOperation(service);
            
            callerActivity.showLoadingDialog();
            
        } else {
            // Show a Message
            Toast t = Toast.makeText(callerActivity, callerActivity.getString(R.string.share_link_no_support_share_api), Toast.LENGTH_LONG);
            t.show();
            
        }
    }
    
    public void sendDownloadedFile(OCFile file, FileActivity callerActivity) {
        if (file != null) {
            Intent sendIntent = new Intent(android.content.Intent.ACTION_SEND);
            // set MimeType
            sendIntent.setType(file.getMimetype());
            sendIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + file.getStoragePath()));
            sendIntent.putExtra(Intent.ACTION_SEND, true);      // Send Action
            
            // Show dialog, without the own app
            String[] packagesToExclude = new String[] { callerActivity.getPackageName() };
            DialogFragment chooserDialog = ShareLinkToDialog.newInstance(sendIntent, packagesToExclude, file);
            chooserDialog.show(callerActivity.getSupportFragmentManager(), FTAG_CHOOSER_DIALOG);

        } else {
            Log_OC.wtf(TAG, "Trying to send a NULL OCFile");
        }
    }
    
    public static String shortenURL(String longUrl, String apiUrl) {
        String shortenUrl = null;
        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost(apiUrl);
        post.setHeader("Content-Type", "application/json");
        StringEntity json = null;
        try {
            json = new StringEntity("{'longUrl': '" + longUrl + "'}");
            post.setEntity(json);
            Log_OC.i(TAG, "sending HTTP POST to " + apiUrl + " in order to shorten url: " + longUrl);
            HttpResponse response = client.execute(post);
            String responseJson = EntityUtils.toString(response.getEntity());
            JSONObject jObject = new JSONObject(responseJson);
            shortenUrl = jObject.getString("id");
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            client.getConnectionManager().shutdown();
        }
        return shortenUrl;
    }
    
    public long getOpIdWaitingFor() {
        return mWaitingForOpId;
    }

    public void setOpIdWaitingFor(long waitingForOpId) {
        mWaitingForOpId = waitingForOpId;
    }

}
