/* ownCloud Android client application
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2,
 *   as published by the Free Software Foundation.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.owncloud.androidApp.files;

import com.owncloud.androidApp.MainApp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class FullSyncBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        SharedPreferences appPrefs = PreferenceManager.getDefaultSharedPreferences(MainApp.getAppContext());
        boolean autoSyncEnabled = appPrefs.getBoolean("auto_sync", false);
        boolean autoSyncOnWifiOnly = appPrefs.getBoolean("auto_sync_on_wifi", false);
        boolean wifiOn = InstantUploadBroadcastReceiver.isConnectedViaWiFi(context);
        if (autoSyncEnabled && (wifiOn || !autoSyncOnWifiOnly)) {
            Intent syncService = new Intent(context, FullSyncService.class);
            context.startService(syncService);
        }
    }
}