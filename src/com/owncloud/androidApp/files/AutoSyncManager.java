/* ownCloud Android client application
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2,
 *   as published by the Free Software Foundation.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.owncloud.androidApp.files;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;

import com.owncloud.androidApp.MainApp;
import com.owncloud.androidApp.utils.Log_OC;

public class AutoSyncManager {

    public static final int DEFAULT_SYNC_INTERVAL = 120; 
    private static PendingIntent pendingIntent;
    private static final String TAG = AutoSyncManager.class.getSimpleName();

    public static void scheduleAutoSync(int interval) {
        Log_OC.i(TAG, "scheduling auto sync");
        long intervalInMilis = interval * 60 * 1000;
        Context ctx = MainApp.getAppContext();
        AlarmManager alarmManager = (AlarmManager) MainApp.getAppContext().getSystemService(ctx.ALARM_SERVICE);
        if (pendingIntent == null) {
            Intent intent = new Intent(ctx, FullSyncBroadcastReceiver.class);
            pendingIntent = PendingIntent.getBroadcast(ctx, 0, intent, 0);
        } else {
            alarmManager.cancel(pendingIntent);
        }
        alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                SystemClock.elapsedRealtime() + intervalInMilis, intervalInMilis, pendingIntent);
    }

    public static void cancelAutoSync() {
        Log_OC.i(TAG, "canceling auto sync");
        Context ctx = MainApp.getAppContext();
        AlarmManager alarmManager = (AlarmManager) MainApp.getAppContext().getSystemService(ctx.ALARM_SERVICE);
        if (pendingIntent == null) {
            Intent intent = new Intent(ctx, FullSyncBroadcastReceiver.class);
            pendingIntent = PendingIntent.getBroadcast(ctx, 0, intent, 0);
        }
        alarmManager.cancel(pendingIntent);
    }
}
