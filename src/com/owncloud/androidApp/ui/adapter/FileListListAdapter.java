/* ownCloud Android client application
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2,
 *   as published by the Free Software Foundation.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package com.owncloud.androidApp.ui.adapter;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import android.accounts.Account;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.owncloud.androidApp.MainApp;
import com.owncloud.androidApp.R;
import com.owncloud.androidApp.authentication.AccountUtils;
import com.owncloud.androidApp.datamodel.FileDataStorageManager;
import com.owncloud.androidApp.datamodel.OCFile;
import com.owncloud.androidApp.files.services.FileDownloader.FileDownloaderBinder;
import com.owncloud.androidApp.files.services.FileUploader.FileUploaderBinder;
import com.owncloud.androidApp.ui.activity.TransferServiceGetter;
import com.owncloud.androidApp.ui.fragment.OCFileListFragment;
import com.owncloud.androidApp.utils.DisplayUtils;


/**
 * This Adapter populates a ListView with all files and folders in an ownCloud
 * instance.
 * 
 * 
 */
public class FileListListAdapter extends BaseAdapter implements ListAdapter {
    private Context mContext;
    private OCFile mFile = null;
    private Vector<OCFile> mFiles = null;
    private FileDataStorageManager mStorageManager;
    private Account mAccount;
    private TransferServiceGetter mTransferServiceGetter;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    private LayoutInflater inflator;
    private OCFileListFragment fragment;
    
    public FileListListAdapter(Context context, OCFileListFragment fragment, TransferServiceGetter transferServiceGetter) {
        mContext = context;
        mAccount = AccountUtils.getCurrentOwnCloudAccount(mContext);
        mTransferServiceGetter = transferServiceGetter;
        options = new DisplayImageOptions.Builder()
        .showImageOnLoading(R.drawable.file_image)
        .cacheInMemory(true)
        .cacheOnDisk(true)
        .build();
        imageLoader = ImageLoader.getInstance();
        inflator = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.fragment = fragment;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return true;
    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }

    @Override
    public int getCount() {
        return mFiles != null ? mFiles.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        if (mFiles == null || mFiles.size() <= position)
            return null;
        return mFiles.get(position);
    }

    @Override
    public long getItemId(int position) {
        if (mFiles == null || mFiles.size() <= position)
            return 0;
        return mFiles.get(position).getFileId();
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }
    
    private static class ViewHolder {
        public ImageView fileIcon;
        public TextView fileName;
        public ImageView localStateView;
//        public TextView fileSizeV;
        public TextView lastModV;
        public ImageView checkBoxV;
        public ImageView shareIcon;
        public ImageView popup;
       }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        View view = convertView;
        final ViewHolder holder;
        if (view == null) {
            view = inflator.inflate(R.layout.list_item, null);
            holder = new ViewHolder();
            holder.fileIcon = (ImageView) view.findViewById(R.id.imageView1);
            holder.fileName = (TextView) view.findViewById(R.id.Filename);
            holder.localStateView = (ImageView) view.findViewById(R.id.imageView2);
//            holder.fileSizeV = (TextView) view.findViewById(R.id.file_size);
            holder.lastModV = (TextView) view.findViewById(R.id.last_mod);
            holder.checkBoxV = (ImageView) view.findViewById(R.id.custom_checkbox);
            holder.shareIcon = (ImageView) view.findViewById(R.id.shareIcon);
            holder.popup = (ImageView) view.findViewById(R.id.context_item);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
    
        if (mFiles != null && mFiles.size() > position) {
            OCFile file = mFiles.get(position);
            String name = file.getFileName();

            holder.fileName.setText(name);
            if (MainApp.isProVersion() && file.isImage() && file.isDown()) {
                    String imageUri = "file:///" + file.getStoragePath();
                    imageLoader.displayImage(imageUri, holder.fileIcon, options, animateFirstListener);
            } else {
                holder.fileIcon.setImageResource(DisplayUtils.getResourceId(file.getMimetype(), file.getFileName()));
            }

            FileDownloaderBinder downloaderBinder = mTransferServiceGetter.getFileDownloaderBinder();
            FileUploaderBinder uploaderBinder = mTransferServiceGetter.getFileUploaderBinder();
            if (downloaderBinder != null && downloaderBinder.isDownloading(mAccount, file)) {
//                // Remove etag for parent, if file is a keep_in_sync
//                if (file.keepInSync()) {
//                    OCFile parent = mFileActivity.getStorageManager().getFileById(file.getParentId());
//                    parent.setEtag("");
//                    mFileActivity.getStorageManager().saveFile(parent);
//                }
                holder.localStateView.setImageResource(R.drawable.downloading_file_indicator);
                holder.localStateView.setVisibility(View.VISIBLE);
            } else if (uploaderBinder != null && uploaderBinder.isUploading(mAccount, file)) {
                holder.localStateView.setImageResource(R.drawable.uploading_file_indicator);
                holder.localStateView.setVisibility(View.VISIBLE);
            } else if (file.isDown()) {
                holder.localStateView.setImageResource(R.drawable.local_file_indicator);
                holder.localStateView.setVisibility(View.VISIBLE);
            } else {
                holder.localStateView.setVisibility(View.INVISIBLE);
            }
            String humanReadableTime = DateUtils.getRelativeDateTimeString(mContext,
                    file.getModificationTimestamp(), DateUtils.SECOND_IN_MILLIS, DateUtils.WEEK_IN_MILLIS,
                    DateUtils.FORMAT_ABBREV_ALL).toString();
            if (!file.isFolder()) {
//                holder.fileSizeV.setVisibility(View.VISIBLE);
//                holder.fileSizeV.setText(DisplayUtils.bytesToHumanReadable(file.getFileLength()));
                holder.lastModV.setVisibility(View.VISIBLE);

                holder.lastModV.setText(DisplayUtils.bytesToHumanReadable(file.getFileLength()) + ", "
                        + mContext.getString(R.string.filedetails_modified) + " " + humanReadableTime);
                ListView parentList = (ListView) parent;
                if (parentList.getChoiceMode() == ListView.CHOICE_MODE_NONE) {
                    holder.checkBoxV.setVisibility(View.GONE);
                } else {
                    if (parentList.isItemChecked(position)) {
                        holder.checkBoxV.setImageResource(android.R.drawable.checkbox_on_background);
                    } else {
                        holder.checkBoxV.setImageResource(android.R.drawable.checkbox_off_background);
                    }
                    holder.checkBoxV.setVisibility(View.VISIBLE);
                }

            } else {
//                holder.fileSizeV.setVisibility(View.INVISIBLE);
                // fileSizeV.setText(DisplayUtils.bytesToHumanReadable(file.getFileLength()));
                holder.lastModV.setVisibility(View.VISIBLE);
                holder.lastModV.setText(mContext.getString(R.string.filedetails_modified) + " " + humanReadableTime);
                holder.checkBoxV.setVisibility(View.GONE);
            }

            if (file.isShareByLink()) {
                holder.shareIcon.setVisibility(View.VISIBLE);
            } else {
                holder.shareIcon.setVisibility(View.INVISIBLE);
            }
            
            holder.popup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int[] values = new int[2];
                    v.getLocationOnScreen(values);
                    fragment.showPopup(fragment.getActivity(), new Point(values[0], values[1]), position);
                }
            });
        }

        return view;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isEmpty() {
        return (mFiles == null || mFiles.isEmpty());
    }

    /**
     * Change the adapted directory for a new one
     * @param directory                 New file to adapt. Can be NULL, meaning "no content to adapt".
     * @param updatedStorageManager     Optional updated storage manager; used to replace mStorageManager if is different (and not NULL)
     */
    public void swapDirectory(OCFile directory, FileDataStorageManager updatedStorageManager) {
        mFile = directory;
        if (updatedStorageManager != null && updatedStorageManager != mStorageManager) {
            mStorageManager = updatedStorageManager;
            mAccount = AccountUtils.getCurrentOwnCloudAccount(mContext);
        }
        if (mStorageManager != null) {
            mFiles = mStorageManager.getFolderContent(mFile);
        } else {
            mFiles = null;
        }
        notifyDataSetChanged();
    }
    
    private static class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

        static final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                boolean firstDisplay = !displayedImages.contains(imageUri);
                if (firstDisplay) {
                    FadeInBitmapDisplayer.animate(imageView, 500);
                    displayedImages.add(imageUri);
                }
            }
        }
    }
    
}
