package com.owncloud.androidApp.ui.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.owncloud.androidApp.AnalyticsTracker;
import com.owncloud.androidApp.R;
import com.owncloud.androidApp.datamodel.OCFile;
import com.owncloud.androidApp.files.services.FileDownloader;
import com.owncloud.androidApp.ui.preview.PreviewImageActivity;

public class ImageGridActivity extends FileActivity {
    public static final String IMAGES = "images";
    private ArrayList<OCFile> images;
    private HashMap<String, String> imageLocalPaths;
    private DisplayImageOptions options;
    private AbsListView listView;
    private ImageLoader imageLoader = ImageLoader.getInstance();
    private DownloadImageFinishReceiver downloadFinishReceiver;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_image_grid);

        Bundle bundle = getIntent().getExtras();
        ArrayList<OCFile> files = bundle.getParcelableArrayList(IMAGES);
        images = new ArrayList<OCFile>(files.size());
        imageLocalPaths = new HashMap<String, String>(files.size());
        for (OCFile file : files) {
            if (file.isImage()) {
                images.add(file);
                imageLocalPaths.put(file.getRemotePath(), file.getStoragePath());
            }
        }

        options = new DisplayImageOptions.Builder().
                showImageOnLoading(R.drawable.file_image)
                .showImageOnFail(R.drawable.file)
                .showImageForEmptyUri(R.drawable.file)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

        listView = (GridView) findViewById(R.id.gridview);
        ((GridView) listView).setAdapter(new ImageAdapter());
        listView.refreshDrawableState();
        
        listView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startImagePreview(images.get(position));
            }
        });
        
        registerForContextMenu(listView);
        listView.setOnCreateContextMenuListener(this);
        AnalyticsTracker.trackActivityImageGridActivity();
    }
    
    @Override
    protected void onResume() {
        super.onResume();
        
        // Listen for download messages
        IntentFilter downloadIntentFilter = new IntentFilter(FileDownloader.getDownloadAddedMessage());
        downloadIntentFilter.addAction(FileDownloader.getDownloadFinishMessage());
        downloadFinishReceiver = new DownloadImageFinishReceiver();
        registerReceiver(downloadFinishReceiver, downloadIntentFilter);
        
        refreshGrid();
    }
    
    @Override
    protected void onPause() {
        super.onPause();
        if (downloadFinishReceiver != null) {
            unregisterReceiver(downloadFinishReceiver);
            downloadFinishReceiver = null;
        }
    }
    
    private void refreshGrid() {
        // refresh list view
        ((ImageAdapter)listView.getAdapter()).notifyDataSetChanged();
        listView.invalidate();
    }
    
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = ImageGridActivity.this.getMenuInflater();
        inflater.inflate(R.menu.file_actions_menu, menu);
        AdapterContextMenuInfo info = (AdapterContextMenuInfo) menuInfo;
        OCFile targetFile = images.get(info.position);
        List<Integer> toHide = new ArrayList<Integer>();
        List<Integer> toDisable = new ArrayList<Integer>();

        MenuItem item = null;
        // contextual menu for regular files

        // context menu
        toHide.add(R.id.action_download_file);
        toHide.add(R.id.action_open_file_with);
        toHide.add(R.id.action_show_image_tiles);
        toHide.add(R.id.action_sync_file);
        toHide.add(R.id.action_cancel_upload);
        toHide.add(R.id.action_cancel_download);
        toHide.add(R.id.action_rename_file);
        toHide.add(R.id.action_remove_file);
        toHide.add(R.id.action_send_file);
        toHide.add(R.id.action_see_details);

        // Options shareLink
        if (!targetFile.isShareByLink()) {
            toHide.add(R.id.action_unshare_file);
        }

        for (int i : toHide) {
            item = menu.findItem(i);
            if (item != null) {
                item.setVisible(false);
                item.setEnabled(false);
            }
        }

        for (int i : toDisable) {
            item = menu.findItem(i);
            if (item != null) {
                item.setEnabled(false);
            }
        }
    }
    
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
        OCFile mTargetFile = images.get(info.position);
        setFile(mTargetFile);
        int itemId = item.getItemId();
        if (itemId == R.id.action_share_file) {
            ImageGridActivity.this.getFileOperationsHelper().shareFileWithLink(mTargetFile, ImageGridActivity.this);
            return true;
        } else if (itemId == R.id.action_share_file_qr) {
            ImageGridActivity.this.getFileOperationsHelper().shareFileWithLinkAsQR(mTargetFile, ImageGridActivity.this);
            return true;
        } else if (itemId == R.id.action_unshare_file) {
            ImageGridActivity.this.getFileOperationsHelper().unshareFileWithLink(mTargetFile, ImageGridActivity.this);
            return true;
        } else {
            return super.onContextItemSelected(item);
        }
    }

    private void startImagePreview(OCFile file) {
        Intent showDetailsIntent = new Intent(this, PreviewImageActivity.class);
        showDetailsIntent.putExtra(FileActivity.EXTRA_FILE, file);
        showDetailsIntent.putExtra(FileActivity.EXTRA_ACCOUNT, getAccount());
        startActivity(showDetailsIntent);
    }

    static class ViewHolder {
        ImageView imageView;
        ProgressBar progressBar;
    }

    public class ImageAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return images.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;
            View view = convertView;
            if (view == null) {
                view = getLayoutInflater().inflate(R.layout.item_grid_image, parent, false);
                holder = new ViewHolder();
                assert view != null;
                holder.imageView = (ImageView) view.findViewById(R.id.grid_image);
//                holder.progressBar = (ProgressBar) view.findViewById(R.id.progress);
                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }
            String localPath = imageLocalPaths.get(images.get(position).getRemotePath());
            String imageUri = "file:///" + localPath;
            
            if (localPath != null && localPath.length() != 0) {
                imageLoader.displayImage(imageUri, holder.imageView, options, new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                        // holder.progressBar.setProgress(0);
                        // holder.progressBar.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        // holder.progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        // holder.progressBar.setVisibility(View.GONE);
                    }
                }, new ImageLoadingProgressListener() {
                    @Override
                    public void onProgressUpdate(String imageUri, View view, int current, int total) {
                        // holder.progressBar.setProgress(Math.round(100.0f *
                        // current / total));
                    }
                });
            } else {
                holder.imageView.setImageDrawable(getResources().getDrawable(R.drawable.file));
            }

            return view;
        }
    }
    
    
    
    /**
     * Class waiting for broadcast events from the {@link FielDownloader} service.
     * 
     * Updates the UI when a download is finished, provided that it is relevant for the
     * current folder.
     */
    private class DownloadImageFinishReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            boolean sameAccount = isSameAccount(context, intent);
            String downloadedRemotePath = intent.getStringExtra(FileDownloader.EXTRA_REMOTE_PATH);
            boolean success = intent.getBooleanExtra(FileDownloader.EXTRA_DOWNLOAD_RESULT, false);
            boolean isDescendant = isLocalFile(downloadedRemotePath);

            if (success && sameAccount && isDescendant) {
                imageLocalPaths.put(downloadedRemotePath, intent.getStringExtra(FileDownloader.EXTRA_FILE_PATH));
                refreshGrid();
            }

            removeStickyBroadcast(intent);
        }

        private boolean isLocalFile(String downloadedRemotePath) {
            return !imageLocalPaths.isEmpty() && downloadedRemotePath != null
                    && imageLocalPaths.containsKey(downloadedRemotePath);
        }

        private boolean isSameAccount(Context context, Intent intent) {
            String accountName = intent.getStringExtra(FileDownloader.ACCOUNT_NAME);
            return (accountName != null && getAccount() != null && accountName.equals(getAccount().name));
        }
    }
}
