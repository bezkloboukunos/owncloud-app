package com.owncloud.androidApp.ui.activity;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

public class GridItemView extends ImageView {

    public GridItemView(Context context) {
        super(context);
        setAdjustViewBounds(true);
        setScaleType(ScaleType.CENTER_CROP);
    }
    
    public GridItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GridItemView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
    
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
      super.onMeasure(widthMeasureSpec, heightMeasureSpec);
      int width = getMeasuredWidth();
      setMeasuredDimension(width, width);
    }

}
