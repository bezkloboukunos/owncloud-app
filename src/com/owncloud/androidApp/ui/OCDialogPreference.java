/* ownCloud Android client application
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2,
 *   as published by the Free Software Foundation.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.owncloud.androidApp.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.DialogPreference;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.owncloud.androidApp.MainApp;
import com.owncloud.androidApp.R;
import com.owncloud.androidApp.files.AutoSyncManager;

public class OCDialogPreference extends DialogPreference {
    private EditText syncIntervalView;

    public OCDialogPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public OCDialogPreference(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onBindView(View view) {
        super.onBindView(view);
        TextView titleView = (TextView) view.findViewById(android.R.id.title);
        titleView.setSingleLine(false);
        titleView.setMaxLines(3);
        titleView.setEllipsize(null);
    }

    @Override
    protected void onBindDialogView(View view) {
        super.onBindDialogView(view);
        syncIntervalView = (EditText) view.findViewById(R.id.sync_interval_txt);
        SharedPreferences appPrefs = PreferenceManager.getDefaultSharedPreferences(MainApp.getAppContext());
        int interval = appPrefs.getInt("auto_sync_interval", AutoSyncManager.DEFAULT_SYNC_INTERVAL);
        syncIntervalView.setText(String.valueOf(interval));
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);
        setSummary(MainApp.getAppContext().getString(R.string.current_value) + ": " + syncIntervalView.getText());
        if (positiveResult) {
            SharedPreferences.Editor appPrefsE = PreferenceManager.getDefaultSharedPreferences(MainApp.getAppContext())
                    .edit();
            int newInterval = Integer.parseInt(syncIntervalView.getText().toString());
            appPrefsE.putInt("auto_sync_interval", newInterval);
            appPrefsE.commit();
            SharedPreferences appPrefs = PreferenceManager.getDefaultSharedPreferences(MainApp.getAppContext());
            int oldInterval = appPrefs.getInt("auto_sync_interval", AutoSyncManager.DEFAULT_SYNC_INTERVAL);
            if (newInterval != oldInterval) {
                AutoSyncManager.scheduleAutoSync(newInterval);
            }
        }
    }
}