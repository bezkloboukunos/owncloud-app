/* ownCloud Android client application
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2,
 *   as published by the Free Software Foundation.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package com.owncloud.androidApp.ui.fragment;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import android.accounts.Account;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.owncloud.android.lib.common.operations.OnRemoteOperationListener;
import com.owncloud.android.lib.common.operations.RemoteOperation;
import com.owncloud.android.lib.common.operations.RemoteOperationResult;
import com.owncloud.android.lib.common.operations.RemoteOperationResult.ResultCode;
import com.owncloud.androidApp.AnalyticsTracker;
import com.owncloud.androidApp.MainApp;
import com.owncloud.androidApp.R;
import com.owncloud.androidApp.authentication.AccountUtils;
import com.owncloud.androidApp.datamodel.FileDataStorageManager;
import com.owncloud.androidApp.datamodel.OCFile;
import com.owncloud.androidApp.files.services.FileObserverService;
import com.owncloud.androidApp.files.services.FileDownloader.FileDownloaderBinder;
import com.owncloud.androidApp.files.services.FileUploader.FileUploaderBinder;
import com.owncloud.androidApp.operations.RemoveFileOperation;
import com.owncloud.androidApp.operations.RenameFileOperation;
import com.owncloud.androidApp.operations.SynchronizeFileOperation;
import com.owncloud.androidApp.ui.activity.ConflictsResolveActivity;
import com.owncloud.androidApp.ui.activity.FileDisplayActivity;
import com.owncloud.androidApp.ui.activity.ImageGridActivity;
import com.owncloud.androidApp.ui.activity.TransferServiceGetter;
import com.owncloud.androidApp.ui.adapter.FileListListAdapter;
import com.owncloud.androidApp.ui.dialog.EditNameDialog;
import com.owncloud.androidApp.ui.dialog.EditNameDialog.EditNameDialogListener;
import com.owncloud.androidApp.ui.fragment.ConfirmationDialogFragment.ConfirmationDialogFragmentListener;
import com.owncloud.androidApp.ui.preview.PreviewImageFragment;
import com.owncloud.androidApp.ui.preview.PreviewMediaFragment;
import com.owncloud.androidApp.utils.DisplayUtils;
import com.owncloud.androidApp.utils.Log_OC;

/**
 * A Fragment that lists all files and folders in a given path.
 *  
 */
public class OCFileListFragment extends ExtendedListFragment implements EditNameDialogListener, ConfirmationDialogFragmentListener, OnRemoteOperationListener {
    
    private static final String TAG = OCFileListFragment.class.getSimpleName();

    private static final String MY_PACKAGE = OCFileListFragment.class.getPackage() != null ? OCFileListFragment.class.getPackage().getName() : "com.owncloud.android.ui.fragment";
    private static final String EXTRA_FILE = MY_PACKAGE + ".extra.FILE";

    private static final String KEY_INDEXES = "INDEXES";
    private static final String KEY_FIRST_POSITIONS= "FIRST_POSITIONS";
    private static final String KEY_TOPS = "TOPS";
    private static final String KEY_HEIGHT_CELL = "HEIGHT_CELL";
    
    private OCFileListFragment.ContainerActivity mContainerActivity;
    
    private OCFile mFile = null;
    private FileListListAdapter mAdapter;
    
    private Handler mHandler;
    private OCFile mTargetFile;

    // Save the state of the scroll in browsing
    private ArrayList<Integer> mIndexes;
    private ArrayList<Integer> mFirstPositions;
    private ArrayList<Integer> mTops;
    
    private PopupWindow popup;
    private volatile int clickX = 0;
    private volatile int clickY = 0;

    private int mHeightCell = 0;
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log_OC.e(TAG, "onAttach");
        try {
            mContainerActivity = (ContainerActivity) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement " + OCFileListFragment.ContainerActivity.class.getSimpleName());
        }
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        mList.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int topRowVerticalPosition = (mList == null || mList.getChildCount() == 0) ? 0 : mList.getChildAt(0)
                        .getTop();
                mContainerActivity.getSwipeRefreshLayout().setEnabled(topRowVerticalPosition >= 0);
            }
        });
        return v;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log_OC.e(TAG, "onActivityCreated() start");
        mAdapter = new FileListListAdapter(getActivity(), this, mContainerActivity);
        if (savedInstanceState != null) {
            mFile = savedInstanceState.getParcelable(EXTRA_FILE);
            mIndexes = savedInstanceState.getIntegerArrayList(KEY_INDEXES);
            mFirstPositions = savedInstanceState.getIntegerArrayList(KEY_FIRST_POSITIONS);
            mTops = savedInstanceState.getIntegerArrayList(KEY_TOPS);
            mHeightCell = savedInstanceState.getInt(KEY_HEIGHT_CELL);
            
        } else {
            mIndexes = new ArrayList<Integer>();
            mFirstPositions = new ArrayList<Integer>();
            mTops = new ArrayList<Integer>();
            mHeightCell = 0;
            
        }
        
        setListAdapter(mAdapter);
        
        registerForContextMenu(getListView());
        getListView().setOnCreateContextMenuListener(this);  
        
        getListView().setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    clickX = (int) event.getRawX();
                    clickY = (int) event.getRawX();
                }
                return false;
            }
        });
        
        mHandler = new Handler();

    }
    
    /**
     * Saves the current listed folder.
     */
    @Override
    public void onSaveInstanceState (Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(EXTRA_FILE, mFile);
        outState.putIntegerArrayList(KEY_INDEXES, mIndexes);
        outState.putIntegerArrayList(KEY_FIRST_POSITIONS, mFirstPositions);
        outState.putIntegerArrayList(KEY_TOPS, mTops);
        outState.putInt(KEY_HEIGHT_CELL, mHeightCell);
    }
    
    /**
     * Call this, when the user presses the up button.
     * 
     * Tries to move up the current folder one level. If the parent folder was removed from the database, 
     * it continues browsing up until finding an existing folders.
     * 
     * return       Count of folder levels browsed up.
     */
    public int onBrowseUp() {
        OCFile parentDir = null;
        int moveCount = 0;
        
        if(mFile != null){
            FileDataStorageManager storageManager = mContainerActivity.getStorageManager();
            
            String parentPath = null;
            if (mFile.getParentId() != FileDataStorageManager.ROOT_PARENT_ID) {
                parentPath = new File(mFile.getRemotePath()).getParent();
                parentPath = parentPath.endsWith(OCFile.PATH_SEPARATOR) ? parentPath : parentPath + OCFile.PATH_SEPARATOR;
                parentDir = storageManager.getFileByPath(parentPath);
                moveCount++;
            } else {
                parentDir = storageManager.getFileByPath(OCFile.ROOT_PATH);    // never returns null; keep the path in root folder
            }
            while (parentDir == null) {
                parentPath = new File(parentPath).getParent();
                parentPath = parentPath.endsWith(OCFile.PATH_SEPARATOR) ? parentPath : parentPath + OCFile.PATH_SEPARATOR;
                parentDir = storageManager.getFileByPath(parentPath);
                moveCount++;
            }   // exit is granted because storageManager.getFileByPath("/") never returns null
            mFile = parentDir;           
        }
        
        if (mFile != null) {
            mFile = mContainerActivity.getStorageManager().getFileById(mFile.getFileId());
            listDirectory(mFile);

            mContainerActivity.startSyncFolderOperation(mFile);
            
            // restore index and top position
            restoreIndexAndTopPosition();
            
        }   // else - should never happen now
   
        return moveCount;
    }
    
    /*
     * Restore index and position
     */
    private void restoreIndexAndTopPosition() {
        if (mIndexes.size() > 0) {  
            // needs to be checked; not every browse-up had a browse-down before 
            
            int index = mIndexes.remove(mIndexes.size() - 1);
            
            int firstPosition = mFirstPositions.remove(mFirstPositions.size() -1);
            
            int top = mTops.remove(mTops.size() - 1);
            
            mList.setSelectionFromTop(firstPosition, top);
            
            // Move the scroll if the selection is not visible
            int indexPosition = mHeightCell*index;
            int height = mList.getHeight();
            
            if (indexPosition > height) {
                if (android.os.Build.VERSION.SDK_INT >= 11)
                {
                    mList.smoothScrollToPosition(index); 
                }
                else if (android.os.Build.VERSION.SDK_INT >= 8)
                {
                    mList.setSelectionFromTop(index, 0);
                }
                
            }
        }
    }
    
    /*
     * Save index and top position
     */
    private void saveIndexAndTopPosition(int index) {
        
        mIndexes.add(index);
        
        int firstPosition = mList.getFirstVisiblePosition();
        mFirstPositions.add(firstPosition);
        
        View view = mList.getChildAt(0);
        int top = (view == null) ? 0 : view.getTop() ;

        mTops.add(top);
        
        // Save the height of a cell
        mHeightCell = (view == null || mHeightCell != 0) ? mHeightCell : view.getHeight();
    }
    
    
    @Override
    public void onItemClick(AdapterView<?> l, View v, int position, long id) {
//        if (isContextPopupClicked(v, clickX, clickY)) {
//            int[] values = new int[2];
//            v.getLocationOnScreen(values);
//            showPopup(getActivity(), new Point(values[0], values[1]), position);
//        } else {
            OCFile file = (OCFile) mAdapter.getItem(position);
            if (file != null) {
                if (file.isFolder()) {
                    // update state and view of this fragment
                    listDirectory(file);
                    // then, notify parent activity to let it update its state
                    // and
                    // view,
                    // and other fragments
                    mContainerActivity.onBrowsedDownTo(file);
                    // save index and top position
                    saveIndexAndTopPosition(position);
                } else { // / Click on a file
                    if (PreviewImageFragment.canBePreviewed(file)) {
                        // preview image - it handles the download, if needed
                        mContainerActivity.startImagePreview(file);
                    } else if (file.isDown()) {
                        if (PreviewMediaFragment.canBePreviewed(file)) {
                            // media preview
                            mContainerActivity.startMediaPreview(file, 0, true);
                        } else {
                            FileDisplayActivity activity = (FileDisplayActivity) getSherlockActivity();
                            activity.getFileOperationsHelper().openFile(file, activity);
                        }

                    } else {
                        // automatic download, preview on finish
                        mContainerActivity.startDownloadForPreview(file);
                    }
                }
            } else {
                Log_OC.d(TAG, "Null object in ListAdapter!!");
            }
//        }
    }
        
    public void showPopup(final Activity context, Point p, int position) {
        // Inflate the popup_layout.xml
        LinearLayout viewGroup = (LinearLayout) context.findViewById(R.id.popup_layout);
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.popup_menu, viewGroup);

        // Creating the PopupWindow
        popup = new PopupWindow(context);
        popup.setContentView(layout);
        popup.setWidth(LinearLayout.LayoutParams.MATCH_PARENT);
        popup.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
        popup.setFocusable(true);

        // Some offset to align the popup a bit to the left, and a bit down,
        // relative to button's position.
        int OFFSET_X = 0;
        int OFFSET_Y = 55;
        
        DisplayMetrics displaymetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int screenHeight = displaymetrics.heightPixels;
        if (p.y + OFFSET_Y + layout.getHeight() + 200 > screenHeight) {
            OFFSET_Y = -OFFSET_Y - layout.getHeight() - 90;
            ((ImageView) layout.findViewById(R.id.pop_arrow2)).setVisibility(View.VISIBLE);
            ((ImageView) layout.findViewById(R.id.pop_arrow1)).setVisibility(View.GONE);
        }

        // Clear the default translucent background
        popup.setBackgroundDrawable(new BitmapDrawable());

        // Displaying the popup at the specified location, + offsets.
        popup.showAtLocation(layout, Gravity.NO_GRAVITY, p.x + OFFSET_X, p.y + OFFSET_Y);
        
        final OCFile file = (OCFile) mAdapter.getItem(position);
        mTargetFile = file;

        TextView link = (TextView) layout.findViewById(R.id.pop_text2);
        link.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                FileDisplayActivity activity = (FileDisplayActivity) getSherlockActivity();
                activity.getFileOperationsHelper().shareFileWithLink(file, activity);
            }
        });

        TextView qr = (TextView) layout.findViewById(R.id.pop_text3);
        qr.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                FileDisplayActivity activity = (FileDisplayActivity) getSherlockActivity();
                activity.getFileOperationsHelper().shareFileWithLinkAsQR(file, activity);
            }
        });

        final TextView delete = (TextView) layout.findViewById(R.id.pop_text4);
        delete.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                int messageStringId = R.string.confirmation_remove_alert;
                int posBtnStringId = R.string.confirmation_remove_remote;
                int neuBtnStringId = -1;
                if (file.isFolder()) {
                    messageStringId = R.string.confirmation_remove_folder_alert;
                    posBtnStringId = R.string.confirmation_remove_remote_and_local;
                    neuBtnStringId = R.string.confirmation_remove_folder_local;
                } else if (file.isDown()) {
                    posBtnStringId = R.string.confirmation_remove_remote_and_local;
                    neuBtnStringId = R.string.confirmation_remove_local;
                }
                ConfirmationDialogFragment confDialog = ConfirmationDialogFragment.newInstance(messageStringId,
                        new String[] { file.getFileName() }, posBtnStringId, neuBtnStringId,
                        R.string.common_cancel);
                confDialog.setOnConfirmationListener(OCFileListFragment.this);
                confDialog.show(getFragmentManager(), FileDetailFragment.FTAG_CONFIRMATION);
            }
        });
//        delete.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                glowOnTouch(R.drawable.delete, delete);
//                return false;
//            }
//        });

        final TextView rename = (TextView) layout.findViewById(R.id.pop_text5);
        rename.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                String fileName = file.getFileName();
                int extensionStart = file.isFolder() ? -1 : fileName.lastIndexOf(".");
                int selectionEnd = (extensionStart >= 0) ? extensionStart : fileName.length();
                EditNameDialog dialog = EditNameDialog.newInstance(getString(R.string.rename_dialog_title), fileName,
                        0, selectionEnd, OCFileListFragment.this);
                dialog.show(getFragmentManager(), EditNameDialog.TAG);
            }
        });
        
        TextView details = (TextView) layout.findViewById(R.id.pop_text6);
        TextView tiles = (TextView) layout.findViewById(R.id.pop_text1);
        if (file.isFolder()) {
            details.setVisibility(View.GONE);
            tiles.setVisibility(View.VISIBLE);
            tiles.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    popup.dismiss();
                    FileDataStorageManager storageManager = mContainerActivity.getStorageManager();
                    if (storageManager != null) {
                        Intent showTilesIntent = new Intent(getActivity(), ImageGridActivity.class);
                        showTilesIntent.putExtra(ImageGridActivity.IMAGES,
                                new ArrayList<OCFile>(storageManager.getFolderContent(file)));
                        startActivity(showTilesIntent);
                    }
                }
            });
        } else {
            tiles.setVisibility(View.GONE);
            details.setVisibility(View.VISIBLE);
            details.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    popup.dismiss();
                    ((FileFragment.ContainerActivity) getActivity()).showDetails(file);
                }
            });
        }

        // ShapeDrawable.ShaderFactory sf = new ShapeDrawable.ShaderFactory() {
        // @Override
        // public Shader resize(int width, int height) {
        // int end = getResources().getColor(R.color.filelist_icon_backgorund);
        // int middle = getResources().getColor(R.color.divider_middle);
        // LinearGradient lg = new LinearGradient(0, 0, width, height,
        // new int[]{end, middle, middle, end},
        // new float[]{0,.125f,.875f,1}, Shader.TileMode.REPEAT);
        // return lg;
        // }
        // };
        //
        // PaintDrawable drawable =new PaintDrawable();
        // drawable.setShape(new RectShape());
        // drawable.setShaderFactory(sf);
        //
        // ImageView stripe1 = (ImageView)
        // layout.findViewById(R.id.pop_stripe1);
        // stripe1.setImageDrawable(drawable);
        // stripe1.setLayoutParams(new LayoutParams(100, 1));
        //
        // ImageView stripe2 = (ImageView)
        // layout.findViewById(R.id.pop_stripe2);
        // stripe2.setImageDrawable(drawable);

    }
    
//    private void glowOnTouch(int originalDrawable, TextView textView) {
//     // An added margin to the initial image
//        int margin = 24;
//        int halfMargin = margin / 2;
//
//        // the glow radius
//        int glowRadius = 16;
//
//        // the glow color
//        int glowColor = Color.rgb(70, 255, 80);
//
//        // The original image to use
//        Bitmap src = BitmapFactory.decodeResource(getResources(), originalDrawable);
//
//        // extract the alpha from the source image
//        Bitmap alpha = src.extractAlpha();
//
//        // The output bitmap (with the icon + glow)
//        Bitmap bmp = Bitmap.createBitmap(src.getWidth() + margin,
//                src.getHeight() + margin, Bitmap.Config.ARGB_8888);
//
//        // The canvas to paint on the image
//        Canvas canvas = new Canvas(bmp);
//
//        Paint paint = new Paint();
//        paint.setColor(glowColor);
//
//        // outer glow
//        paint.setMaskFilter(new BlurMaskFilter(glowRadius, Blur.OUTER));
//        canvas.drawBitmap(alpha, halfMargin, halfMargin, paint);
//
//        // original icon
//        canvas.drawBitmap(src, halfMargin, halfMargin, null);
//        textView.setCompoundDrawablesWithIntrinsicBounds(null, new BitmapDrawable(getResources(), bmp), null, null);
//    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void onCreateContextMenu (ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.file_actions_menu, menu);
        AdapterContextMenuInfo info = (AdapterContextMenuInfo) menuInfo;
        OCFile targetFile = (OCFile) mAdapter.getItem(info.position);
        List<Integer> toShow = new ArrayList<Integer>();
        List<Integer> toHide = new ArrayList<Integer>();    
        List<Integer> toDisable = new ArrayList<Integer>(); 
//        String instantUploadName = MainApp.getAppContext().getString(R.string.instant_upload_path);
//        instantUploadName = instantUploadName.substring(1);
        
        MenuItem item = null;
        if (targetFile.isFolder()) {
            // contextual menu for folders
            toHide.add(R.id.action_open_file_with);
            toHide.add(R.id.action_download_file);
            toHide.add(R.id.action_cancel_download);
            toHide.add(R.id.action_cancel_upload);
            toHide.add(R.id.action_sync_file);
            toHide.add(R.id.action_see_details);
            toHide.add(R.id.action_send_file);
            
            if (!MainApp.isProVersion()) {
                toHide.add(R.id.action_download_folder);
                toHide.add(R.id.action_sync_folder);
                toHide.add(R.id.action_stop_sync_folder);
            } else {
                if (targetFile.keepInSync()) {
                    toShow.add(R.id.action_stop_sync_folder);
                } else {
                    toShow.add(R.id.action_sync_folder);
                }
            }
            
            if (    mContainerActivity.getFileDownloaderBinder().isDownloading(AccountUtils.getCurrentOwnCloudAccount(getActivity()), targetFile) ||
                    mContainerActivity.getFileUploaderBinder().isUploading(AccountUtils.getCurrentOwnCloudAccount(getActivity()), targetFile)           ) {
                toDisable.add(R.id.action_rename_file);
                toDisable.add(R.id.action_remove_file);   
            }
            if (!MainApp.isProVersion() || !DisplayUtils.isImage(targetFile.getFileName())) {
                toHide.add(R.id.action_show_image_tiles);
            }
        } else {
            // contextual menu for regular files
            
            // new design: 'download' and 'open with' won't be available anymore in context menu
            toHide.add(R.id.action_download_file);
            toHide.add(R.id.action_download_folder);
            toHide.add(R.id.action_open_file_with);
            
            toHide.add(R.id.action_show_image_tiles);
            
            if (targetFile.isDown()) {
                toHide.add(R.id.action_cancel_download);
                toHide.add(R.id.action_cancel_upload);
                
            } else {
                toHide.add(R.id.action_sync_file);
            }
            if ( mContainerActivity.getFileDownloaderBinder().isDownloading(AccountUtils.getCurrentOwnCloudAccount(getActivity()), targetFile)) {
                toHide.add(R.id.action_cancel_upload);
                toDisable.add(R.id.action_rename_file);
                toDisable.add(R.id.action_remove_file);
                    
            } else if ( mContainerActivity.getFileUploaderBinder().isUploading(AccountUtils.getCurrentOwnCloudAccount(getActivity()), targetFile)) {
                toHide.add(R.id.action_cancel_download);
                toDisable.add(R.id.action_rename_file);
                toDisable.add(R.id.action_remove_file);
                    
            } else {
                toHide.add(R.id.action_cancel_download);
                toHide.add(R.id.action_cancel_upload);
            }
        }
        
        // Options shareLink
        if (!targetFile.isShareByLink()) {
            toHide.add(R.id.action_unshare_file);
        }

        // Send file
        boolean sendEnabled = getString(R.string.send_files_to_other_apps).equalsIgnoreCase("on");
        if (!sendEnabled) {
            toHide.add(R.id.action_send_file);
        }
        
        for (int i : toHide) {
            item = menu.findItem(i);
            if (item != null) {
                item.setVisible(false);
                item.setEnabled(false);
            }
        }
        
        for (int i : toShow) {
            item = menu.findItem(i);
            if (item != null) {
                item.setVisible(true);
                item.setEnabled(true);
            }
        }
        
        for (int i : toDisable) {
            item = menu.findItem(i);
            if (item != null) {
                item.setEnabled(false);
            }
        }
        AnalyticsTracker.trackContextFileMenuOpened();
    }
    
    
    /**
     * {@inhericDoc}
     */
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
        mTargetFile = (OCFile) mAdapter.getItem(info.position);
        int itemId = item.getItemId();
        if (itemId == R.id.action_share_file) {
            FileDisplayActivity activity = (FileDisplayActivity) getSherlockActivity();
            activity.getFileOperationsHelper().shareFileWithLink(mTargetFile, activity);
            return true;
        } else if (itemId == R.id.action_share_file_qr) {
            FileDisplayActivity activity = (FileDisplayActivity) getSherlockActivity();
            activity.getFileOperationsHelper().shareFileWithLinkAsQR(mTargetFile, activity);
            return true;
        } else if (itemId == R.id.action_unshare_file) {
            FileDisplayActivity activity = (FileDisplayActivity) getSherlockActivity();
            activity.getFileOperationsHelper().unshareFileWithLink(mTargetFile, activity);
            return true;
        } else if (itemId == R.id.action_show_image_tiles) {
            FileDataStorageManager storageManager = mContainerActivity.getStorageManager();
            if (storageManager != null) {
                Intent showTilesIntent = new Intent(getActivity(), ImageGridActivity.class);
                showTilesIntent.putExtra(ImageGridActivity.IMAGES,
                        new ArrayList<OCFile>(storageManager.getFolderContent(mTargetFile)));
                startActivity(showTilesIntent);
            }
            return true;
        } else if (itemId == R.id.action_rename_file) {
            String fileName = mTargetFile.getFileName();
            int extensionStart = mTargetFile.isFolder() ? -1 : fileName.lastIndexOf(".");
            int selectionEnd = (extensionStart >= 0) ? extensionStart : fileName.length();
            EditNameDialog dialog = EditNameDialog.newInstance(getString(R.string.rename_dialog_title), fileName, 0,
                    selectionEnd, this);
            dialog.show(getFragmentManager(), EditNameDialog.TAG);
            return true;
        } else if (itemId == R.id.action_remove_file) {
            int messageStringId = R.string.confirmation_remove_alert;
            int posBtnStringId = R.string.confirmation_remove_remote;
            int neuBtnStringId = -1;
            if (mTargetFile.isFolder()) {
                messageStringId = R.string.confirmation_remove_folder_alert;
                posBtnStringId = R.string.confirmation_remove_remote_and_local;
                neuBtnStringId = R.string.confirmation_remove_folder_local;
            } else if (mTargetFile.isDown()) {
                posBtnStringId = R.string.confirmation_remove_remote_and_local;
                neuBtnStringId = R.string.confirmation_remove_local;
            }
            ConfirmationDialogFragment confDialog = ConfirmationDialogFragment.newInstance(messageStringId,
                    new String[] { mTargetFile.getFileName() }, posBtnStringId, neuBtnStringId, R.string.common_cancel);
            confDialog.setOnConfirmationListener(this);
            confDialog.show(getFragmentManager(), FileDetailFragment.FTAG_CONFIRMATION);
            return true;
        } else if (itemId == R.id.action_sync_file) {
            Account account = AccountUtils.getCurrentOwnCloudAccount(getSherlockActivity());
            RemoteOperation operation = new SynchronizeFileOperation(mTargetFile, null,
                    mContainerActivity.getStorageManager(), account, true, getSherlockActivity());
            operation.execute(account, getSherlockActivity(), mContainerActivity, mHandler, getSherlockActivity());
            ((FileDisplayActivity) getSherlockActivity()).showLoadingDialog();
            return true;
            
        } else if (itemId == R.id.action_sync_folder || itemId == R.id.action_stop_sync_folder) {
            boolean sync = itemId == R.id.action_sync_folder;
            toggleSync(sync, mTargetFile);
            return true;
            
        } else if (itemId == R.id.action_cancel_download) {
            FileDownloaderBinder downloaderBinder = mContainerActivity.getFileDownloaderBinder();
            Account account = AccountUtils.getCurrentOwnCloudAccount(getActivity());
            if (downloaderBinder != null && downloaderBinder.isDownloading(account, mTargetFile)) {
                downloaderBinder.cancel(account, mTargetFile);
                listDirectory();
                mContainerActivity.onTransferStateChanged(mTargetFile, false, false);
            }
            return true;
        } else if (itemId == R.id.action_cancel_upload) {
            FileUploaderBinder uploaderBinder = mContainerActivity.getFileUploaderBinder();
            Account account = AccountUtils.getCurrentOwnCloudAccount(getActivity());
            if (uploaderBinder != null && uploaderBinder.isUploading(account, mTargetFile)) {
                uploaderBinder.cancel(account, mTargetFile);
                listDirectory();
                mContainerActivity.onTransferStateChanged(mTargetFile, false, false);
            }
            return true;
        } else if (itemId == R.id.action_see_details) {
            ((FileFragment.ContainerActivity) getActivity()).showDetails(mTargetFile);
            return true;
        } else if (itemId == R.id.action_send_file) {
            // Obtain the file
            if (!mTargetFile.isDown()) { // Download the file
                Log_OC.d(TAG, mTargetFile.getRemotePath() + " : File must be downloaded");
                mContainerActivity.startDownloadForSending(mTargetFile);
            } else {
                FileDisplayActivity activity = (FileDisplayActivity) getSherlockActivity();
                activity.getFileOperationsHelper().sendDownloadedFile(mTargetFile, activity);
            }
            return true;
        } else if (itemId == R.id.action_download_folder) {
            FileDisplayActivity activity = (FileDisplayActivity) getSherlockActivity();
            activity.requestForDownload(mTargetFile);
            return true;
        } else {
            return super.onContextItemSelected(item);
        }
    }


    /**
     * Use this to query the {@link OCFile} that is currently
     * being displayed by this fragment
     * @return The currently viewed OCFile
     */
    public OCFile getCurrentFile(){
        return mFile;
    }
    
    /**
     * Calls {@link OCFileListFragment#listDirectory(OCFile)} with a null parameter
     */
    public void listDirectory(){
        listDirectory(null);
    }
    
    /**
     * Lists the given directory on the view. When the input parameter is null,
     * it will either refresh the last known directory. list the root
     * if there never was a directory.
     * 
     * @param directory File to be listed
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public void listDirectory(OCFile directory) {
        FileDataStorageManager storageManager = mContainerActivity.getStorageManager();
        if (storageManager != null) {

            // Check input parameters for null
            if(directory == null){
                if(mFile != null){
                    directory = mFile;
                } else {
                    directory = storageManager.getFileByPath("/");
                    if (directory == null) return; // no files, wait for sync
                }
            }
        
        
            // If that's not a directory -> List its parent
            if(!directory.isFolder()){
                Log_OC.w(TAG, "You see, that is not a directory -> " + directory.toString());
                directory = storageManager.getFileById(directory.getParentId());
            }
            mAdapter.swapDirectory(directory, storageManager);
            if (mFile == null || !mFile.equals(directory)) {
                mList.setSelectionFromTop(0, 0);
            }
            mFile = directory;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
                ((FileDisplayActivity) getSherlockActivity()).invalidateOptionsMenu();
            }
        }
    }
    
    
    
    /**
     * Interface to implement by any Activity that includes some instance of FileListFragment
     * 
     */
    public interface ContainerActivity extends TransferServiceGetter, OnRemoteOperationListener {

        /**
         * Callback method invoked when a the user browsed into a different folder through the list of files
         *  
         * @param file
         */
        public void onBrowsedDownTo(OCFile folder);

        public void startDownloadForPreview(OCFile file);

        public void startMediaPreview(OCFile file, int i, boolean b);

        public void startImagePreview(OCFile file);
        
        public void startSyncFolderOperation(OCFile folder);
        
        void refeshListOfFilesFragment();
        
        /**
         * Getter for the current DataStorageManager in the container activity
         */
        public FileDataStorageManager getStorageManager();
        
        SwipeRefreshLayout getSwipeRefreshLayout();
        
        
        /**
         * Callback method invoked when a the 'transfer state' of a file changes.
         * 
         * This happens when a download or upload is started or ended for a file.
         * 
         * This method is necessary by now to update the user interface of the double-pane layout in tablets
         * because methods {@link FileDownloaderBinder#isDownloading(Account, OCFile)} and {@link FileUploaderBinder#isUploading(Account, OCFile)}
         * won't provide the needed response before the method where this is called finishes. 
         * 
         * TODO Remove this when the transfer state of a file is kept in the database (other thing TODO)
         * 
         * @param file          OCFile which state changed.
         * @param downloading   Flag signaling if the file is now downloading.
         * @param uploading     Flag signaling if the file is now uploading.
         */
        public void onTransferStateChanged(OCFile file, boolean downloading, boolean uploading);

        void startDownloadForSending(OCFile file);
        
    }
    
    
    @Override
    public void onDismiss(EditNameDialog dialog) {
        if (dialog.getResult()) {
            String newFilename = dialog.getNewFilename();
            Log_OC.d(TAG, "name edit dialog dismissed with new name " + newFilename);
            RemoteOperation operation = new RenameFileOperation(mTargetFile, 
                                                                AccountUtils.getCurrentOwnCloudAccount(getActivity()), 
                                                                newFilename, 
                                                                mContainerActivity.getStorageManager());
            operation.execute(AccountUtils.getCurrentOwnCloudAccount(getSherlockActivity()), getSherlockActivity(), mContainerActivity, mHandler, getSherlockActivity());
            ((FileDisplayActivity) getActivity()).showLoadingDialog();
        }
    }

    
    @Override
    public void onConfirmation(String callerTag) {
        if (callerTag.equals(FileDetailFragment.FTAG_CONFIRMATION)) {
            if (mContainerActivity.getStorageManager().getFileById(mTargetFile.getFileId()) != null) {
                RemoteOperation operation = new RemoveFileOperation( mTargetFile, 
                                                                    true, 
                                                                    mContainerActivity.getStorageManager());
                operation.execute(AccountUtils.getCurrentOwnCloudAccount(getSherlockActivity()), getSherlockActivity(), mContainerActivity, mHandler, getSherlockActivity());
                
                ((FileDisplayActivity) getActivity()).showLoadingDialog();
            }
        }
    }
    
    @Override
    public void onNeutral(String callerTag) {
        mContainerActivity.getStorageManager().removeFile(mTargetFile, false, true);    // TODO perform in background task / new thread
        listDirectory();
        mContainerActivity.onTransferStateChanged(mTargetFile, false, false);
    }
    
    @Override
    public void onCancel(String callerTag) {
        Log_OC.d(TAG, "REMOVAL CANCELED");
    }
    
    
    private void toggleSync(boolean sync, OCFile file) {
        file.setKeepInSync(sync);
        mContainerActivity.getStorageManager().saveFile(file);
        if (!file.isFolder()) {
            // / register the OCFile instance in the observer service to monitor
            // local updates;
            // / if necessary, the file is download
            Intent intent = new Intent(getActivity().getApplicationContext(), FileObserverService.class);
            intent.putExtra(FileObserverService.KEY_FILE_CMD, (sync ? FileObserverService.CMD_ADD_OBSERVED_FILE
                    : FileObserverService.CMD_DEL_OBSERVED_FILE));
            intent.putExtra(FileObserverService.KEY_CMD_ARG_FILE, file);
            intent.putExtra(FileObserverService.KEY_CMD_ARG_ACCOUNT,
                    AccountUtils.getCurrentOwnCloudAccount(getActivity()));
            getActivity().startService(intent);

            if (file.keepInSync()) {
                Log.e("XXX", "synchronizeFile(" + file.getFileName() + ")");
                synchronizeFile(file);
            }
        } else {
            Vector<OCFile> files = mContainerActivity.getStorageManager().getFolderContent(file);
            // recursively sync its content
            for (OCFile fileInFolder : files) {
                toggleSync(sync, fileInFolder);
            }
        }
    }
    
    private void synchronizeFile(OCFile file) {
        Account account = AccountUtils.getCurrentOwnCloudAccount(getActivity());
        FileDownloaderBinder downloaderBinder = mContainerActivity.getFileDownloaderBinder();
        FileUploaderBinder uploaderBinder = mContainerActivity.getFileUploaderBinder();
        if (downloaderBinder != null && downloaderBinder.isDownloading(account, file)) {
            downloaderBinder.cancel(account, file);
            Log.e("XXX", "OCFileListFr.synchronizeFile1");
        } else if (uploaderBinder != null && uploaderBinder.isUploading(account, file)) {
            uploaderBinder.cancel(account, file);
            Log.e("XXX", "OCFileListFr.synchronizeFile2");
            if (!file.fileExists()) {
                // TODO make something better
                Log.e("XXX", "OCFileListFr.synchronizeFile3");
                ((FileDisplayActivity) getActivity()).cleanSecondFragment();
            }
        } else {
            RemoteOperation remoteOperation = new SynchronizeFileOperation(file, null,
                    mContainerActivity.getStorageManager(), account, true, getActivity());
            remoteOperation.execute(account, getSherlockActivity(), this, mHandler, getSherlockActivity());
        }
    }
    
    
    private void onSynchronizeFileOperationFinish(SynchronizeFileOperation operation, RemoteOperationResult result) {
        ((FileDisplayActivity) getActivity()).dismissLoadingDialog();
        OCFile file = operation.getLocalFile();

        if (!result.isSuccess()) {
            if (result.getCode() == ResultCode.SYNC_CONFLICT) {
                Log.e("XXX", "CONFLICT");
                Intent i = new Intent(getActivity(), ConflictsResolveActivity.class);
                i.putExtra(ConflictsResolveActivity.EXTRA_FILE, file);
                i.putExtra(ConflictsResolveActivity.EXTRA_ACCOUNT, AccountUtils.getCurrentOwnCloudAccount(getActivity()));
                startActivity(i);
            } 
        } else {
            if (operation.transferWasRequested()) {
                mContainerActivity.refeshListOfFilesFragment();    // this is not working; FileDownloader won't do NOTHING at all until this method finishes, so 
                                                            // checking the service to see if the file is downloading results in FALSE
            } else {
                Toast msg = Toast.makeText(getActivity(), R.string.sync_file_nothing_to_do_msg, Toast.LENGTH_LONG); 
                msg.show();
            }
        }
    }

    @Override
    public void onRemoteOperationFinish(RemoteOperation caller, RemoteOperationResult result) {
        onSynchronizeFileOperationFinish((SynchronizeFileOperation)caller, result);
    }
}
