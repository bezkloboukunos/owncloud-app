package com.owncloud.androidApp;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

public class AnalyticsTracker {
    
    public static void trackActivityFileDisplayActivity() {
        Tracker tracker = MainApp.getTracker(MainApp.TrackerName.APP_TRACKER);
        tracker.setScreenName("com.owncloud.androidApp.ui.activity.FileDisplayActivity");
        tracker.send(new HitBuilders.AppViewBuilder().build());
        tracker.setScreenName(null);
    }
    
    public static void trackActivityImageGridActivity() {
        Tracker tracker = MainApp.getTracker(MainApp.TrackerName.APP_TRACKER);
        tracker.setScreenName("com.owncloud.androidApp.ui.activity.ImageGridActivity");
        tracker.send(new HitBuilders.AppViewBuilder().build());
        tracker.setScreenName(null);
    }
    
    public static void trackContextFileMenuOpened() {
        Tracker tracker = MainApp.getTracker(MainApp.TrackerName.APP_TRACKER);
        tracker.setScreenName("contextFileMenu");
        tracker.send(new HitBuilders.AppViewBuilder().build());
        tracker.setScreenName(null);
    }
    
    public static void trackShareLink(boolean shortened, boolean qr) {
        Tracker tracker = MainApp.getTracker(MainApp.TrackerName.APP_TRACKER);
        tracker.send(new HitBuilders.EventBuilder()
               .setCategory("Actions")
               .setAction("enableShortUrl")
               .setLabel("Enable short URLs")
               .build());
        tracker.setScreenName(null);
    }
    
}
