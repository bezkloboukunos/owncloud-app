/* ownCloud Android client application
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2,
 *   as published by the Free Software Foundation.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package owncloud.pro;

import com.owncloud.androidApp.MainApp;
/**
 * Main Application of the project
 * 
 * Contains methods to build the "static" strings. These strings were before constants in different classes
 * 
 */
public class MainAppPro extends MainApp {
	
	static {
		setPro(true);
	}
    
    public static boolean isProVersion() {
        return true;
    }
}
